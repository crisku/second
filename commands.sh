#!/bin/sh
docker container stop redis_container_1 redis_container_1 redis_container_2 redis_container_3 redis_container_4 redis_container_5 redis_container_6
docker container rm redis_container_1 redis_container_1 redis_container_2 redis_container_3 redis_container_4 redis_container_5 redis_container_6

docker build -t crisk_redis_compiled .
#docker run -p 6379:6379 crisk_redis_compiled:latest /usr/local/etc/redis/redis.conf
docker-compose up -d
# docker exec -it 08df007058d4 redis-cli
# docker exec -it redis_container_1 sh

#	docker network create myNetwork
docker network connect myNetwork redis_container_1
docker network connect myNetwork redis_container_2
docker network connect myNetwork redis_container_3
docker network connect myNetwork redis_container_4
docker network connect myNetwork redis_container_5
docker network connect myNetwork redis_container_6

#redis-cli --cluster create 172.19.0.2:6379 172.19.0.3:6378 --cluster-replicas 1
redis-cli --cluster create 172.19.0.2:6379 172.19.0.3:6379 172.19.0.4:6379 172.19.0.5:6379 172.19.0.6:6379 172.19.0.6:6379 --cluster-replicas 1